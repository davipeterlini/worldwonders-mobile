package com.ciandt.cursoandroid.worldwondersapp.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ciandt.cursoandroid.worldwondersapp.entity.User;
import com.ciandt.cursoandroid.worldwondersapp.infrastructure.Constants;
import com.ciandt.cursoandroid.worldwondersapp.integrator.GeneralIntegrator;

public class LoginManager {
//Aula 5 - Exercicio 3
    private GeneralIntegrator mGeneralIntegrator;
    private SharedPreferences mSharedPreferences;
    

    public LoginManager(final Context context) {
        mGeneralIntegrator = new GeneralIntegrator(context);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }
    //Aula 5 - Exercicio 5
    public Boolean isUserLogged() {

        Boolean isUserLogged = Boolean.FALSE;

        String userEmail = mSharedPreferences.getString(Constants.Bundle.BUNDLE_USER_EMAIL, null);
        if (userEmail != null) {
            isUserLogged = Boolean.TRUE;
        }

        return isUserLogged;
    }
    //Aula 5 - Exercicio 3
    public User loginUser(final String userEmail, final String userPassword) {
        User user = mGeneralIntegrator.loginUserOnBackend(userEmail, userPassword);

        if (user != null){
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(Constants.Bundle.BUNDLE_USER_EMAIL, userEmail);
            editor.putString(Constants.Bundle.BUNDLE_USER_PASSWORD, userPassword);
            editor.commit();
        }

        return user;
    }
    //Aula 5 - Exercicio 4
    public void logoutUser() {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(Constants.Bundle.BUNDLE_USER_NAME);
        editor.remove(Constants.Bundle.BUNDLE_USER_EMAIL);
        editor.commit();
    }

}