package com.ciandt.cursoandroid.worldwondersapp.manager;

import android.content.Context;

import com.ciandt.cursoandroid.worldwondersapp.entity.User;
import com.ciandt.cursoandroid.worldwondersapp.integrator.GeneralIntegrator;

//Aula 5 - Exercício 2
public class RegisterManager {

    private GeneralIntegrator mGeneralIntegrator;

    public RegisterManager(final Context context) {
        mGeneralIntegrator = new GeneralIntegrator(context);
    }

    public Boolean registerUser(final User user) {
        return mGeneralIntegrator.registerUserOnBackend(user);
    }
}