package com.ciandt.cursoandroid.worldwondersapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.location.GpsStatus;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.adapter.PlaceCursorAdapter;
import com.ciandt.cursoandroid.worldwondersapp.contentprovider.WorldWondersContentProvider;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.ciandt.cursoandroid.worldwondersapp.manager.DatabaseManager;

/**
 * Created by davi on 10/12/14.
 */

//Aula 7 - Exercicio 5
public class PlaceListFragment extends Fragment {

    public Activity mActivity;
    private ListView mListView;
    private DatabaseManager mDatabaseManager;
    private PlaceCursorAdapter mPlaceCursorAdapter;
    private OnPlaceSelectedListener mListener;

    public PlaceListFragment() {
        // Required empty public constructor
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mListener = (OnPlaceSelectedListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPlaceCursorAdapter = new PlaceCursorAdapter(mActivity, null);
        mDatabaseManager = new DatabaseManager(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_place_list, container, false);

        mListView = (ListView) viewRoot.findViewById(R.id.mainListview);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mListView.getAdapter().getItem(position);
            }
        });

        getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
                CursorLoader cursorLoader = mDatabaseManager.query(WorldWondersContentProvider.PLACE_CONTENT_URI,
                        PlaceTable.ALL_COLUMNS, null, null, null);
                return cursorLoader;
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                mPlaceCursorAdapter.swapCursor(data);
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {
                mPlaceCursorAdapter.swapCursor(null);
            }
        });

        return viewRoot;
    }

    public interface OnPlaceSelectedListener {
        public void onPlaceSelected(Place place);
    }

}


