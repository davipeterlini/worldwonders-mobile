package com.ciandt.cursoandroid.worldwondersapp.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentBreadCrumbs;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.adapter.PlaceCursorAdapter;
import com.ciandt.cursoandroid.worldwondersapp.contentprovider.WorldWondersContentProvider;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.ciandt.cursoandroid.worldwondersapp.fragment.PlaceDetailFragment;
import com.ciandt.cursoandroid.worldwondersapp.fragment.PlaceListFragment;
import com.ciandt.cursoandroid.worldwondersapp.listener.IntegratorOperatorCallback;
import com.ciandt.cursoandroid.worldwondersapp.manager.DatabaseManager;
import com.ciandt.cursoandroid.worldwondersapp.manager.LoginManager;
import com.ciandt.cursoandroid.worldwondersapp.manager.PlaceManager;

import java.util.List;

import static com.ciandt.cursoandroid.worldwondersapp.fragment.PlaceDetailFragment.newInstance;


public class MainActivity extends Activity implements PlaceListFragment.OnPlaceSelectedListener {

    private LoginManager mloginManager;
    //Aula 6 - Exercicio 7
    //private ListView mlistView;
	//private DatabaseManager mDatabaseManager;
    //private PlaceCursorAdapter mPlaceCursorAdapter;
    //Aula 7 - Exercicio 5
    private ListView mListViewFeed;
    //Aula 8 - Exercicio 6
    private PlaceManager mplaceManager;
    //Aula 7 - 10
    private FrameLayout mframeLayout;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Aula 5 - Exercicio 5
        mloginManager = new LoginManager(this);
        if (!mloginManager.isUserLogged())
            actionClickLogout();

        //Aula 6 - Exercicio 7
        //Aula 7 - Exercicio 5 -Movido para PlaceListFragment.java
//        mlistView = (ListView) findViewById(R.id.mainListview);
//        mPlaceCursorAdapter = new PlaceCursorAdapter(this, null);
//        mDatabaseManager = new DatabaseManager(this);
//        mlistView.setAdapter(mPlaceCursorAdapter);
//
//        getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
//            @Override
//            public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
//                CursorLoader cursorLoader = mDatabaseManager.query(WorldWondersContentProvider.PLACE_CONTENT_URI,
//                        PlaceTable.ALL_COLUMNS, null, null, null);
//                return cursorLoader;
//            }
//
//            @Override
//            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//                mPlaceCursorAdapter.swapCursor(data);
//            }
//
//            @Override
//            public void onLoaderReset(Loader<Cursor> loader) {
//                mPlaceCursorAdapter.swapCursor(null);
//            }
//        });

        //Aula 8 - Exercicio 6
        //Aula 8 - Exercicio 7 -
        mplaceManager = new PlaceManager();
        //mplaceManager.getAllPlace();

        //Aula 8 - Exercicio 9
        mplaceManager.getAllPlace(new IntegratorOperatorCallback<List<Place>>() {
            @Override
            public void onOperationCompleteSuccess(List<Place> places) {
                android.util.Log.e("vai trem", places.toString());
            }

            @Override
            public void onOperationCompleteError() {

            }
        });

        //Aula 5 - Exercicio 11
        /*List<Place> list = databaseManager.query(WorldWondersContentProvider.PLACE_CONTENT_URI ,
                                                    PlaceTable.ALL_COLUMNS,
                                                    null ,
                                                    null ,
                                                    null);
        for(int i=0;i< list.size();i++) {
            Place place = list.get(i);
            android.util.Log.e("TEST: ", place.toString());
        }*/

        //Aula - Exercicio 7
        mframeLayout = (FrameLayout) findViewById(R.id.frame_layout_container_place_detail);
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            //Primeira Vez
            PlaceDetailFragment placeDetailFragment = new PlaceDetailFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.frame_layout_container_place_detail, placeDetailFragment);
            ft.commit();
        } else {
            mframeLayout.setVisibility(View.GONE);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //Aula 4 - Exercicio XXX
        int id = item.getItemId();
        if (id == R.id.action_about) {
            return true;
        }
        if (id == R.id.action_logout) {
            actionClickLogout();
        }
        return super.onOptionsItemSelected(item);
    }

    //Aula 5 - Exercicio 5
    private void actionClickLogout(){
        mloginManager.logoutUser();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //Aula 7 - Exercicio 8
    @Override
    public void onPlaceSelected(final Place place) {
//        mListViewFeed.getAdapter().getItem();

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // Tablet
            PlaceDetailFragment placeDetailFragment = PlaceDetailFragment.newInstance(place);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout_container_place_detail, placeDetailFragment);
            ft.commit();
        } else {
            // SmartPhone -  após selecionar um item da lista -
            // callback (onPlaceSelected(place) da MainActivity)
            Intent intent = new Intent(this, PlaceDetailActivity.class);
            intent.putExtra(PlaceDetailFragment.SELECTED_PLACE, place);
            startActivity(intent);

        }

    }

    //Aula 10 - Exercicio 1
    public void dialogSync () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //Configuração
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(R.string.dlg_confirmation_title);
        builder.setMessage(R.string.dlg_confirmation_message);
        builder.setPositiveButton(R.string.dlg_confirmation_button_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Aula 9
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
