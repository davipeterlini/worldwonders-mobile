package com.ciandt.cursoandroid.worldwondersapp.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

/**
 * Created by davi on 03/12/14.
 */

//Aula 6 - Exercicio 6
public class PlaceCursorAdapter extends CursorAdapter {

    TextView nome, descricao, pais;
    ImageView img;
    ProgressBar progressBar;

    public PlaceCursorAdapter(Context context, Cursor c) {
        super(context, c, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View newView = inflater.inflate(R.layout.main_feed_item, parent, false);
        return newView;
    }

    //Aula 6 - Exercicio 7
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        nome        = (TextView)    view.findViewById(R.id.text_view_main_item_name);
        descricao = (TextView) view.findViewById(R.id.main_item_content);
        pais        = (TextView)    view.findViewById(R.id.main_item_country);
        img         = (ImageView)   view.findViewById(R.id.image);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        Place place = new Place(cursor);
        nome.setText(place.getPlaceName());
        descricao.setText(place.getPlaceDescription());
        pais.setText(place.getPlaceCountry());
        progressBar.setVisibility(View.VISIBLE);

        //Aula 6 - Exercicio 10
        UrlImageViewHelper.setUrlDrawable(img, place.getPlaceImageURL(), new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
