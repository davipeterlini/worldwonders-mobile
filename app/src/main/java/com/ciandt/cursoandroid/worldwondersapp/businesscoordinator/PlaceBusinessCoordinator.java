package com.ciandt.cursoandroid.worldwondersapp.businesscoordinator;

import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.ciandt.cursoandroid.worldwondersapp.integrator.PlaceIntegrator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davi on 10/12/14.
 */
public class PlaceBusinessCoordinator {
    public List<Place> getAllPlace(){
        PlaceIntegrator placeIntegrator = new PlaceIntegrator();
        JSONObject jsonObject = placeIntegrator.getAllPlace();

        // parsear o JSON retornar resultado em lista.
        Place place;
        List<Place> list = null;
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray("data");
            list = new ArrayList<Place>();
            for(int i=0; i<jsonArray.length();i++) {
                place = new Place(jsonArray.getJSONObject(i));
                list.add(place);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
