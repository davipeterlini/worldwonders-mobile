package com.ciandt.cursoandroid.worldwondersapp.database.table;

/**
 * Created by davi on 06/11/14.
 */
//Aula 5 - Extra
public interface PlaceTable1 {

    String TABLE_NAME = "place";

    String ID = "_id";
    String PLACE_NAME = "place_name";

    String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " ( "
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + PLACE_NAME + " TEXT" + ")";

    String WHERE_ID_EQUALS = ID + "?";

    String SQL_DROP = "DROP TABLE" + TABLE_NAME;
}