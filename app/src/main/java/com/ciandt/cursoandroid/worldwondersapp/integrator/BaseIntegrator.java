package com.ciandt.cursoandroid.worldwondersapp.integrator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by davi on 10/12/14.
 */

//Aula 8 - Exercicio 3
public class BaseIntegrator {

    public String doGetRequest(String protocol, String host, String path){

        URI uri = null;
        //String varParam1="";
        //List<NameValuePair> params = new ArrayList<NameValuePair>();
        //params.add(new BasicNameValuePair("parametro", varParam1));

        try {
            uri = URIUtils.createURI(protocol, host, 0, path, null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        try {
            HttpGet get = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            String responseStr = EntityUtils.toString(entity);
            return responseStr;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
