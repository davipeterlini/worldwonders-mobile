package com.ciandt.cursoandroid.worldwondersapp.integrator;

import com.ciandt.cursoandroid.worldwondersapp.infrastructure.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by davi on 10/12/14.
 */

//Aula 8 - Exercicio 4
public class PlaceIntegrator extends BaseIntegrator {

    public JSONObject getAllPlace() {

        JSONObject jsonObj = null;
        String jsonString =  doGetRequest (
                Constants.HTTP_PROTOCOL,
                Constants.Integrator.WorldWondersApi.HOST,
                Constants.Integrator.WorldWondersApi.WORLD_WONDERS_LIST);

        try {
            jsonObj = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
}
