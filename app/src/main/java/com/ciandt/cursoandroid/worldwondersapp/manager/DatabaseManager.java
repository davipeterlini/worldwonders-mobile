package com.ciandt.cursoandroid.worldwondersapp.manager;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;

import com.ciandt.cursoandroid.worldwondersapp.contentprovider.WorldWondersContentProvider;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davi on 13/11/14.
 */
//Aula 5 - Exercicio 11
public class DatabaseManager {

    private static Context mContext;

    public DatabaseManager(Context context) {
        this.mContext = context;
    }

    public long insert(final Uri uri,
                       final ContentValues contValues){
        return 1;
    }

    //Aula 6 - Exercicio 5
    public CursorLoader query(final Uri uri,
                              final String[] projection,
                              final String   selection,
                              final String[] selectionArgs,
                              final String   sortOrder){

        CursorLoader cursorLoader = new CursorLoader(mContext,
                uri,projection,selection,selectionArgs,sortOrder);

        return cursorLoader;

    }

    //Aula 5 - Exercicio 11
//    public static List<Place> query(final Uri uri,
//                                    final String[] projection,
//                                    final String selection,
//                                    final String[] selectionArgs,
//                                    final String sortOrder){
//
//        ContentResolver contentResolver = mContext.getContentResolver();
//        Cursor cursor = contentResolver.query(uri,projection,selection,selectionArgs,sortOrder);
//
//        List<Place> list = new ArrayList<Place>();
//
//        while (cursor.moveToNext()) {
//            list.add(new Place(cursor));
//        }
//        cursor.close();
//        return list;
//    }

}
