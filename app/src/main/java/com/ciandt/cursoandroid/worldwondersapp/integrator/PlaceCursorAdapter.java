package com.ciandt.cursoandroid.worldwondersapp.integrator;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.ciandt.cursoandroid.worldwondersapp.R;

/**
 * Created by davi on 03/12/14.
 */
public class PlaceCursorAdapter extends CursorAdapter {


    public PlaceCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View newView = inflater.inflate(R.layout.main_feed_item, parent, false);
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }
}
