package com.ciandt.cursoandroid.worldwondersapp.entity;

import android.database.Cursor;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by davi on 06/11/14.
 */
//Aula 5 - Exercicio 8

public class Place extends BaseEntity implements Serializable {

    private int id;
    private String placeName;
    private String placeCountry;
    private String placeDescription;
    private String placeImageURL;

	//Aula 8 - Exercicio 8 - Continuação
	//Construtor
    public Place(final JSONObject jsonObject){
        this.id                 =  this.getInteger(jsonObject, "id"         );
        this.placeName          =  this.getString (jsonObject, "name"       );
        this.placeCountry       =  this.getString (jsonObject, "country"    );
        this.placeDescription   =  this.getString (jsonObject, "description");
        this.placeImageURL      =  this.getString (jsonObject, "image_url"  );
    }
    
    //Construtor
    public Place(final Cursor cursor) {
        this.id = cursor.getInt(
                cursor.getColumnIndex(PlaceTable.ID));
        this.placeName = cursor.getString(
                cursor.getColumnIndex(PlaceTable.PLACE_NAME));
        this.placeCountry = cursor.getString(
                cursor.getColumnIndex(PlaceTable.PLACE_COUNTRY));
        this.placeDescription = cursor.getString(
                cursor.getColumnIndex(PlaceTable.PLACE_DESCRIPTION));
        this.placeImageURL = cursor.getString(
                cursor.getColumnIndex(PlaceTable.PLACE_IMAGE_URL));
    }

    //Aula 8 - Exercicio 10
    @Override
    public String toString() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("\n").append(id).append("\n").
                append(placeName).append("\n").
                append(placeCountry).append("\n").
                append(placeDescription).append("\n").
                append(placeImageURL);
        return strBuf.toString();
    }

    public int getId() {
        return id;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPlaceCountry() {
        return placeCountry;
    }

    public String getPlaceDescription() {
        return placeDescription;
    }

    public String getPlaceImageURL() {
        return placeImageURL;
    }
}