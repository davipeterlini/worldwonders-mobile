package com.ciandt.cursoandroid.worldwondersapp.manager;

import android.os.AsyncTask;

import com.ciandt.cursoandroid.worldwondersapp.businesscoordinator.PlaceBusinessCoordinator;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.ciandt.cursoandroid.worldwondersapp.integrator.PlaceIntegrator;
import com.ciandt.cursoandroid.worldwondersapp.listener.IntegratorOperatorCallback;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by davi on 10/12/14.
 */

//Aula 8 - Exercicio 5
public class PlaceManager {
    public JSONObject getAllPlace(){
        PlaceIntegrator placeIntegrator = new PlaceIntegrator();
        return placeIntegrator.getAllPlace();
    }

    //Aula 8 - Exercicio 9
    public void getAllPlace(final IntegratorOperatorCallback<List<Place>> callback){

        new AsyncTask<Void, Void, List<Place>>() {
            @Override
            protected List<Place> doInBackground(Void... params) {
                PlaceBusinessCoordinator placeBusinessCoordinator = new PlaceBusinessCoordinator();
                return placeBusinessCoordinator.getAllPlace();
            }

            @Override
            protected void onPostExecute(List<Place> resultList) {
                super.onPostExecute(resultList);
                if (resultList != null) {
                    callback.onOperationCompleteSuccess(resultList);
                } else {
                    callback.onOperationCompleteError();
                }
            }
        }.execute();
    }
}
