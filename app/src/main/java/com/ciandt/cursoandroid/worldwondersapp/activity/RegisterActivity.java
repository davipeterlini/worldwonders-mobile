package com.ciandt.cursoandroid.worldwondersapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.entity.User;
import com.ciandt.cursoandroid.worldwondersapp.manager.RegisterManager;

import java.util.Arrays;


public class RegisterActivity extends Activity {

    private EditText mEditTextName;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private EditText mEditTextPasswordConfirmation;
    private Spinner mSpinnerCountry;
    private AutoCompleteTextView mAutoCompleteLanguage;
    private TextView mTextViewGender;
    private RadioGroup mRadioGroupGender;
    private CheckBox mCheckBoxNotifications;
    private Button mButtonRegister;
    private User user;
    private RegisterManager registerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getActionBar().hide();

        registerManager = new RegisterManager(this);

        LinearLayout layoutRegister = (LinearLayout) findViewById(R.id.linear_layout_register);

        mEditTextName = (EditText) layoutRegister.findViewById(R.id.edit_text_register_name);
        mEditTextEmail = (EditText) layoutRegister.findViewById(R.id.edit_text_register_email);
        mEditTextPassword = (EditText) layoutRegister.findViewById(R.id.edit_text_register_password);
        mEditTextPasswordConfirmation = (EditText) layoutRegister.findViewById(R.id.edit_text_register_password_confirmation);
        mSpinnerCountry = (Spinner) layoutRegister.findViewById(R.id.spinner_register_country);
        mAutoCompleteLanguage = (AutoCompleteTextView) layoutRegister.findViewById(R.id.edit_text_register_language);
        mTextViewGender = (TextView) layoutRegister.findViewById(R.id.text_view_register_gender);
        mRadioGroupGender = (RadioGroup) layoutRegister.findViewById(R.id.radio_group_register_gender);
        mCheckBoxNotifications = (CheckBox) layoutRegister.findViewById(R.id.check_box_register_notifications);
        mButtonRegister = (Button) layoutRegister.findViewById(R.id.button_register_enter);

        if(getIntent().hasExtra("email")){
            String email = getIntent().getStringExtra("email");
            ((EditText) layoutRegister.findViewById(R.id.edit_text_register_email)).setText(email);
        }

        this.setListeners();
    }

    private void setListeners() {

        mButtonRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (validateFields()) {

                    String email = mEditTextEmail.getText().toString();
                    String password = mEditTextPassword.getText().toString();
                    RadioButton genderSelected = (RadioButton) findViewById(mRadioGroupGender.getCheckedRadioButtonId());
                    String gender = genderSelected.getText().toString();

                    user = new User();
                    user.setUserName(mEditTextName.getText().toString());
                    user.setUserEmail(email);
                    user.setUserPassword(password);
                    user.setUserCountry(mSpinnerCountry.getSelectedItem().toString());
                    user.setUserLanguage(mAutoCompleteLanguage.getText().toString());
                    user.setUserGender(gender);

                    if(registerManager.registerUser(user)){
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email",email);
                        bundle.putString("password",password);
                        intent.putExtras(bundle);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
            }
        });

        mSpinnerCountry.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
        
        //Aula 6 - Exercicio 2

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setLanguageCountry();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                setLanguageCountry();
            }
        });

    }

    //Aula 6 - Exercicio 2 e 3
    private void setLanguageCountry () {

        Resources res = getResources();

        String[] array = new String[]{};

        if ("Brasil".equals(mSpinnerCountry.getSelectedItem().toString())) {
            array = res.getStringArray(R.array.languages_pt);
        } else if ("Canadá".equals(mSpinnerCountry.getSelectedItem().toString())) {
            array = res.getStringArray(R.array.languages_ca);
        } else if ("Itália".equals(mSpinnerCountry.getSelectedItem().toString())) {
            array = res.getStringArray(R.array.languages_it);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(RegisterActivity.this,
                android.R.layout.simple_dropdown_item_1line, Arrays.asList(array));
        mAutoCompleteLanguage.setAdapter(arrayAdapter);
    }

    private Boolean validateFields() {

        Boolean isValidFields = Boolean.TRUE;

        if (mEditTextName.getText().toString().isEmpty()) {
            mEditTextName.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        if (mEditTextEmail.getText().toString().isEmpty()) {
            mEditTextEmail.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        if (mEditTextPassword.getText().toString().isEmpty()) {
            mEditTextPassword.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        if (mEditTextPasswordConfirmation.getText().toString().isEmpty()) {
            mEditTextPasswordConfirmation.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        if (!(mEditTextPasswordConfirmation.getText().toString()).equals(mEditTextPasswordConfirmation.getText().toString())) {
            mEditTextPasswordConfirmation.setError(getResources().getString(R.string.msg_password_confirmation));
            isValidFields = Boolean.FALSE;
        }


        if (mRadioGroupGender.getCheckedRadioButtonId() == -1) {
            mTextViewGender.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }


        if (mAutoCompleteLanguage.getText().toString().isEmpty()) {
            mAutoCompleteLanguage.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

//        if (mCheckBoxNotifications.
//            mEditTextPassword.setError(getResources().getString(R.string.msg_required_field));
//            isValidFields = Boolean.FALSE;
//        }

        return isValidFields;
    }

}