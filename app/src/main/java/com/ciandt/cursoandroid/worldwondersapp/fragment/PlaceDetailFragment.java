package com.ciandt.cursoandroid.worldwondersapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.entity.Place;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

/**
 * Created by davi on 10/12/14.
 */

//Aula 7 - Exercicio 2

public class PlaceDetailFragment extends Fragment {

    public static final String SELECTED_PLACE = "selected_place";

    public Activity mActivity;
    private TextView nome, descricao, pais;
    private ImageView img;
    private ProgressBar progressBar;

    public PlaceDetailFragment() {
        // Required empty public constructor
    }

    public static PlaceDetailFragment newInstance(final Place place) {
        PlaceDetailFragment fragment = new PlaceDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(SELECTED_PLACE, place);
        fragment.setArguments(args);
        return fragment;
    }

    public void onAttach(Activity activity) {
        mActivity = activity;
    }

    public void onCreateView(View view, Context context, Cursor cursor) {

        nome        = (TextView)    view.findViewById(R.id.text_view_main_item_name);
        descricao   = (TextView)    view.findViewById(R.id.main_item_content);
        pais        = (TextView)    view.findViewById(R.id.main_item_country);
        img         = (ImageView)   view.findViewById(R.id.image);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        Place place = new Place(cursor);
        nome.setText(place.getPlaceName());
        descricao.setText(place.getPlaceDescription());
        pais.setText(place.getPlaceCountry());
        progressBar.setVisibility(View.VISIBLE);

        UrlImageViewHelper.setUrlDrawable(img, place.getPlaceImageURL(), new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    //Aula 7 - Exercicio 3
    private Place loadPlace() {
        // primeiro, checar se o dado esta no Intent (estamos na PlaceDetailsActivity) - Smartphone
        Place place = (Place) mActivity.getIntent().getSerializableExtra(SELECTED_PLACE);

        if (place == null) {
            // Se nao encontramos no Intent, entao o dado deve estar nos argumentos do
            // Fragment (estamos na MainActivity) - Tablet
            Bundle args = getArguments();
            if (args == null) {
                place = (Place) args.getSerializable(SELECTED_PLACE);
            }
        }
        return place;
    }

}