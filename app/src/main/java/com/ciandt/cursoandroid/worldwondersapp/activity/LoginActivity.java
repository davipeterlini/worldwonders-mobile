package com.ciandt.cursoandroid.worldwondersapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ciandt.cursoandroid.worldwondersapp.R;
import com.ciandt.cursoandroid.worldwondersapp.entity.User;
import com.ciandt.cursoandroid.worldwondersapp.infrastructure.Constants;
import com.ciandt.cursoandroid.worldwondersapp.manager.LoginManager;

public class LoginActivity extends Activity {

    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private Button mButtonLogin;
    private Button mButtonRegister;
    private LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActionBar().hide();

        loginManager = new LoginManager(this);

        LinearLayout layoutLogin = (LinearLayout) findViewById(R.id.linear_layout_login);

        mEditTextEmail = (EditText) layoutLogin.findViewById(R.id.edit_text_login_email);
        mEditTextPassword = (EditText) layoutLogin.findViewById(R.id.edit_text_login_password);
        mButtonLogin = (Button) layoutLogin.findViewById(R.id.button_login_enter);
        mButtonRegister = (Button) layoutLogin.findViewById(R.id.button_login_register);

        this.setListeners();
    }

    private void setListeners() {

        mButtonLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (validateFields()) {

                    String email = mEditTextEmail.getText().toString();
                    String password = mEditTextPassword.getText().toString();

                    loginUser(email, password);
                }
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent register = new Intent(LoginActivity.this, RegisterActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", mEditTextEmail.getText().toString());
                register.putExtras(bundle);
                startActivityForResult(register, Constants.Entity.User.REQUEST_CODE_LOGIN_ACTIVITY);
            }
        });
    }

    private void loginUser(String userEmail, String userPassword){
        User user = loginManager.loginUser(userEmail, userPassword);
        if(user != null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            Toast.makeText(LoginActivity.this, R.string.invalid_login, Toast.LENGTH_SHORT).show();
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.Entity.User.REQUEST_CODE_LOGIN_ACTIVITY) {
                if (data != null) {
                    String email = data.getStringExtra("email");
                    String password = data.getStringExtra("password");
                    loginUser(email, password);
                    finish();
                }
            }
//        } else if (resultCode == Activity.RESULT_CANCEL) {
//
        }
    }

    private Boolean validateFields() {

        Boolean isValidFields = Boolean.TRUE;

        if (mEditTextEmail.getText().toString().isEmpty()) {
            mEditTextEmail.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        if (mEditTextPassword.getText().toString().isEmpty()) {
            mEditTextPassword.setError(getResources().getString(R.string.msg_required_field));
            isValidFields = Boolean.FALSE;
        }

        return isValidFields;
    }

}