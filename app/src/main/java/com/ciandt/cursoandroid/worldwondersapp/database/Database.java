package com.ciandt.cursoandroid.worldwondersapp.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ciandt.cursoandroid.worldwondersapp.contentprovider.WorldWondersContentProvider;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable1;
import com.ciandt.cursoandroid.worldwondersapp.database.table.PlaceTable2;

/**
 * Created by davi on 06/11/14.
 */
//Aula 5 - Exercicio 9

public class Database extends SQLiteOpenHelper {

    public Database(Context context) {
        super(context, "WorldWonders.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(PlaceTable.SQL_CREATE);

        String sqlMock1 = "INSERT INTO place VALUES(1, 'Machu Picchu', 'Peru', " +
                "'Da hora d+ Machu Picchu', 'http://atp.cx/wp-content/uploads/2011/02/The-Pyramid-of-Cheops.jpg');";
        // Teste Aula 6 - Exercicio 11 - //http://atp.cx/wp-content/uploads/2011/02/Machu-Picchu1.jpg
        String sqlMock2 = "INSERT INTO place VALUES(2, 'Chichen Itza', 'Mexico', " +
                "'Da hora d+ Chichen Itza', 'http://www.chichenitza.com/images/chichenitza.jpg');";
        String sqlMock3 = "INSERT INTO place VALUES(3, 'Christ Redeemer', 'Brazil', " +
                "'Da hora d+ Christ Redeemer', 'http://www.thewondersoftheworld.net/images/ChristTheRedeemer1.jpg');";
        String sqlMock4 = "INSERT INTO place VALUES(4, 'Great Wall', 'China', " +
                "'Da hora d+ Great Wall', 'http://img.timeinc.net/time/photoessays/2008/beijing_travel/great_wall_beijing.jpg');";
        String sqlMock5 = "INSERT INTO place VALUES(5, 'Taj Mahal', 'India', " +
                "'Da hora d+ Taj Mahal', 'http://www.tajmahal.com/images/taj_mahal_india.jpg');";
        String sqlMock6 = "INSERT INTO place VALUES(6, 'Petra', 'Jordan', " +
                "'Da hora d+ Petra', 'http://www.culturefocus.com/jordan/pictures/petra-26small.jpg');";
        String sqlMock7 = "INSERT INTO place VALUES(7, 'Colosseum', 'Italy', " +
                "'Da hora d+ Colosseum', 'http://www.altiusdirectory.com/Arts/images/Colosseum.jpg');";

        db.execSQL(sqlMock1);
        db.execSQL(sqlMock2);
        db.execSQL(sqlMock3);
        db.execSQL(sqlMock4);
        db.execSQL(sqlMock5);
        db.execSQL(sqlMock6);
        db.execSQL(sqlMock7);

    }
    //Aula 5 - Extra - para isso é preciso cria a interface da tabela
    @Override
    public void onUpgrade(final SQLiteDatabase db, int oldVersion, int newVersion) {
//        if((oldVersion == 1) && (newVersion == 2)) {
//            // Creating new tables
//            db.execSQL(PlaceTable1.SQL_CREATE);
//            // And altering
//            db.execSQL(PlaceTable.SQL_DROP);
//            db.execSQL(PlaceTable2.SQL_CREATE);
//        } else if (oldVersion == 1 && newVersion == 3) {
//            // Creating new tables
//            db.execSQL(PlaceTable1.SQL_CREATE);
//            // And altering
//            db.execSQL(PlaceTable2.SQL_DROP);
//            db.execSQL(PlaceTable2.SQL_CREATE);
//        } else if (oldVersion == 2 && newVersion == 3) {
//            db.execSQL("ALTER TABLE " + PlaceTable2.TABLE_NAME + " ADD COLUMN " +
//                    PlaceTable2.DESCRIPTION + " TEXT");
//        }
    }

}