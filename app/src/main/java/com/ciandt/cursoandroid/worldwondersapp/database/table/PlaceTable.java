package com.ciandt.cursoandroid.worldwondersapp.database.table;

/**
 * Created by davi on 06/11/14.
 */
//Aula 5 - Exercicio 7
public interface PlaceTable {

    String TABLE_NAME = "place";

    String ID = "_id";
    String PLACE_NAME = "place_name";
    String PLACE_COUNTRY = "place_country";
    String PLACE_DESCRIPTION = "place_description";
    String PLACE_IMAGE_URL = "place_image_url";

    String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " ( "
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + PLACE_NAME         + " TEXT" + ","
            + PLACE_COUNTRY      + " TEXT" + ","
            + PLACE_DESCRIPTION  + " TEXT" + ","
            + PLACE_IMAGE_URL    + " TEXT" + ")";

    String WHERE_ID_EQUALS = "1";

    //Aula 5 - Exercicio 11
    String [] ALL_COLUMNS = new String[] {ID, PLACE_NAME, PLACE_COUNTRY, PLACE_DESCRIPTION, PLACE_IMAGE_URL};

    //Aula 5 - Extra
    String SQL_DROP = "DROP TABLE" + TABLE_NAME;

}